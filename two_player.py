from tkinter import *
from Jeu.Jeu_sur_console_first_step.creer_chiffre import *
import time


global theme_1
global theme_2
global choix_1
global choix_2
global le_index_1
global le_index_2
global root
global value_signe
#root=Tk()

def change_value(value):
    global value_signe
    value_signe=value[0:3]+['+','*','Clean']+value[3:5]+['-','/','Delete']+value[5:7]+['(',')','Valider']
    return value_signe

def list_str(list):
    resulta=''
    for i in list:
        resulta+=str(i)
    return resulta

def change(event):
    global le_index_1
    global le_index_2
    global theme_1
    global theme_2

    global point_1, point_2, time_0, is_valide_1, is_valide_2, choix_1, choix_2

    if is_valide_1:
        pass
    else:
        if event.keysym =='w' and 0<le_index_1-1<16:
            le_index_1-=1
            theme_1.set(value_signe[le_index_1])
        if event.keysym=='s' and 0<le_index_1+1<16:
            le_index_1+=1
            theme_1.set(value_signe[le_index_1])
        if event.keysym=='a'and 0<le_index_1-5<16:
            le_index_1-=5
            theme_1.set(value_signe[le_index_1])
        if event.keysym=='d'and 0<le_index_1+5<16:
            le_index_1+=5
            theme_1.set(value_signe[le_index_1])
        if event.keysym=='space':
            if 'Valider' not in choix_1:
                if value_signe[le_index_1] != 'Delete' and value_signe[le_index_1] != 'Valider' and value_signe[le_index_1] != 'Clean':
                    if value_signe[le_index_1] in choix_1 and value_signe[le_index_1] not in ['+','-','*','/']:
                        pass
                    else:
                        if len(choix_1) != 0:
                            if str(choix_1[-1]).isdigit() and str(value_signe[le_index_1]).isdigit():  # pour eviter deux chiffres consecutifs
                                pass
                            elif choix_1[-1] in ['+','-','*','/'] and value_signe[le_index_1] in ['+','-','*','/']:  # pour eviter deux signes consecutifs
                                pass
                            elif choix_1[-1] == '(' and value_signe[le_index_1] in ['+','-','*','/']:  # il faut un chiffre
                                pass
                            elif choix_1[-1] == ')' and value_signe[le_index_1] not in ['+','-','*','/'] + [')']:  # if faut une signe
                                pass
                            elif choix_1[-1] == 'operation:' and value_signe[le_index_1] in ['+','-','*','/'] + [')']:  # ce ne peut pas etre le premier element dans une equation
                                pass
                            elif str(choix_1[-1]).isdigit() and value_signe[le_index_1] == '(':  # il faut un operation
                                pass
                            elif choix_1[-1] in ['+','-','*','/'] + ['('] and value_signe[le_index_1] == ')':  # il faut un chiffre
                                pass
                            print('joueur1 a choisi:',value_signe[le_index_1])
                            choix_1.append(value_signe[le_index_1])
                            print('le choix_1 est:',choix_1)
                            Label(root, text=list_str(choix_1),width=20).grid(row=6,column=0)
                        else:
                            print('joueur1 a choisi:',value_signe[le_index_1])
                            choix_1.append(value_signe[le_index_1])
                            print('le choix_1 est:',choix_1)
                            Label(root, text=list_str(choix_1),width=20).grid(row=6,column=0)
                if value_signe[le_index_1]== 'Clean':
                    for i in range(len(choix_1)):
                        choix_1.pop(0)
                    Label(root, text=list_str(choix_1),width=20).grid(row=6,column=0)
                    print('le choix_1 est:',choix_1)
                if value_signe[le_index_1]== 'Delete':
                    choix_1.pop(-1)
                    Label(root, text=list_str(choix_1),width=20).grid(row=6,column=0)
                    print('le choix_1 est:',choix_1)
                if value_signe[le_index_1]== 'Valider':
                    try:
                        is_valide_1 = True
                        time_used = time.time() - time_0
                        Label(root,text=str(int(eval(list_str(choix_1)))),width=20).grid(row=6,column=1)
                        point_1 -= abs(int(value_signe[0]) - int(eval((list_str(choix_1)))))
                        point_1 -= time_used//10
                        choix_1.append(int(eval((list_str(choix_1)))))
                        choix_1.append('Valider')
                    except:
                        Label(root,text='Pas valide,try again',width=20).grid(row=6,column=1)
            else:
                pass

    if is_valide_2:
        pass
    else:
        if event.keysym =='Up' and 0<le_index_2-1<16:
            le_index_2-=1
            theme_2.set(value_signe[le_index_2])
        if event.keysym=='Down' and 0<le_index_2+1<16:
            le_index_2+=1
            theme_2.set(value_signe[le_index_2])
        if event.keysym=='Left'and 0<le_index_2-5<16:
            le_index_2-=5
            theme_2.set(value_signe[le_index_2])
        if event.keysym=='Right'and 0<le_index_2+5<16:
            le_index_2+=5
            theme_2.set(value_signe[le_index_2])
        if event.keysym=='Return':
            if 'Valider' not in choix_2:
                if value_signe[le_index_2] != 'Delete' and value_signe[le_index_2] != 'Valider' and value_signe[le_index_2] != 'Clean':
                    if value_signe[le_index_2] in choix_2 and value_signe[le_index_2] not in ['+','-','*','/']:
                        pass
                    else:
                        if len(choix_2) != 0:
                            if str(choix_2[-1]).isdigit() and str(value_signe[le_index_2]).isdigit():  # pour eviter deux chiffres consecutifs
                                pass
                            elif choix_2[-1] in ['+','-','*','/'] and value_signe[le_index_2] in ['+','-','*','/']:  # pour eviter deux signes consecutifs
                                pass
                            elif choix_2[-1] == '(' and value_signe[le_index_2] in ['+','-','*','/']:  # il faut un chiffre
                                pass
                            elif choix_2[-1] == ')' and value_signe[le_index_2] not in ['+','-','*','/'] + [')']:  # if faut une signe
                                pass
                            elif choix_2[-1] == 'operation:' and value_signe[le_index_2] in ['+','-','*','/'] + [')']:  # ce ne peut pas etre le premier element dans une equation
                                pass
                            elif str(choix_2[-1]).isdigit() and value_signe[le_index_2] == '(':  # il faut un operation
                                pass
                            elif choix_2[-1] in ['+','-','*','/'] + ['('] and value_signe[le_index_2] == ')':  # il faut un chiffre
                                pass
                            else:
                                print('joueur2 a choisi:',value_signe[le_index_2])
                                choix_2.append(value_signe[le_index_2])
                                print('le choix_2 est:',choix_2)
                                Label(root, text=list_str(choix_2),width=20).grid(row=6,column=4)
                        else:
                            print('joueur2 a choisi:',value_signe[le_index_2])
                            choix_2.append(value_signe[le_index_2])
                            print('le choix_2 est:',choix_2)
                            Label(root, text=list_str(choix_2),width=20).grid(row=6,column=4)
                if value_signe[le_index_2]== 'Clean':
                    for i in range(len(choix_2)):
                        choix_2.pop(0)
                    Label(root, text=list_str(choix_2),width=20).grid(row=6,column=4)
                    print('le choix_2 est:',choix_2)
                if value_signe[le_index_2]== 'Delete':
                    choix_2.pop(-1)
                    Label(root, text=list_str(choix_2),width=20).grid(row=6,column=4)
                    print('le choix_2 est:',choix_2)
                if value_signe[le_index_2]== 'Valider':
                    try:
                        is_valide_2 = True
                        time_used = time.time() - time_0
                        Label(root,text=str(int(eval(list_str(choix_2)))),width=20).grid(row=6,column=5)
                        point_2 -= abs(value_signe[0] - int(eval((list_str(choix_2)))))
                        point_2 -= time_used//10
                        choix_2.append(int(eval((list_str(choix_2)))))
                        choix_2.append('Valider')
                    except:
                        Label(root,text='Pas valide,Try again',width=20).grid(row=6,column=5)
            else:
                pass


def next_level(go_back_func):
    global is_valide_1, is_valide_2, le_index_1, le_index_2, choix_1, choix_2, niveau, time_0
    niveau+=1
    Label(root,text='Niveau: '+str(niveau),width=20).grid(row=2, column=3)
    Label(root, text='Point Rest:'+str(int(point_1)),font=('Centaur',12)).grid(row=0,column=0)
    Label(root, text='Point Rest:'+str(int(point_2)),font=('Centaur',12)).grid(row=0,column=4)
    if is_valide_1 and is_valide_2:
        is_valide_1 = False
        is_valide_2 = False
        le_index_1 = 1
        le_index_2 = 1
        choix_1=[]
        choix_2=[]
        time_0 = time.time()
        niveau += 1
        chiffre_list = donne_chiffre(niveau)
        change_value(chiffre_list[:7])
        if point_1 > 0 and point_2 > 0:
            for i in range(5):
                Radiobutton(root,
                            text=value_signe[i + 1],
                            variable=theme_1,
                            indicatoron = 0,
                            width=20,
                            value=value_signe[i + 1]).grid(row=i + 1, column=0)
            for i in range(5):
                Radiobutton(root,
                            text=value_signe[i + 6],
                            variable=theme_1,
                            indicatoron = 0,
                            width=20,
                            value=value_signe[i + 6]).grid(row=i + 1, column=1)
            for i in range(5):
                Radiobutton(root,
                            text=value_signe[i + 11],
                            variable=theme_1,
                            indicatoron = 0,
                            width=20,
                            value=value_signe[i + 11]).grid(row=i + 1, column=2)
            Label(root, text='', width=20).grid(row=6, column=0)
            Label(root, text='', width=20).grid(row=6, column=1)

            for i in range(5):
                Radiobutton(root,
                            text=value_signe[i + 1],
                            variable=theme_2,
                            indicatoron = 0,
                            width=20,
                            value=value_signe[i + 1]).grid(row=i + 1, column=4)
            for i in range(5):
                Radiobutton(root,
                            text=value_signe[i + 6],
                            variable=theme_2,
                            indicatoron = 0,
                            width=20,
                            value=value_signe[i + 6]).grid(row=i + 1, column=5)
            for i in range(5):
                Radiobutton(root,
                            text=value_signe[i + 11],
                            variable=theme_2,
                            indicatoron=0,
                            width=20,
                            value=value_signe[i + 11]).grid(row=i + 1, column=6)
            Label(root, text='', width=20).grid(row=6, column=4)
            Label(root, text='', width=20).grid(row=6, column=5)
        else:
            root.destroy()
            result_window = Tk()
            result_window.title('Result')
            if point_2 > point_1:
                text_result = 'Player2 Wins'
            elif point_2 < point_1:
                text_result = 'Player1 Wins'
            else:
                text_result = 'Tie'
            Label(result_window, text=text_result, width=60).grid(columnspan=2)
            def go_back():
                result_window.destroy()
                go_back_func()
            Button(result_window, text='Go Back', command=go_back, width=30).grid(row=1, column=0)
            Button(result_window, text='Quit', command=result_window.destroy, width=30).grid(row=1, column=1)
            result_window.mainloop()


def finish_now(go_back_func):
    global point_1, point_2
    root.destroy()
    result_window = Tk()
    result_window.title('Result')
    if point_2 > point_1:
        text_result = 'Player2 wins'
    elif point_2 < point_1:
        text_result = 'Player1 wins'
    else:
        text_result = 'Tie'
    Label(result_window, text=text_result, width=60).grid(columnspan=2)
    def go_back():
        result_window.destroy()
        go_back_func()
    Button(result_window, text='Go back', command=go_back, width=30).grid(row=1, column=0)
    Button(result_window, text='Quit', command=result_window.destroy, width=30).grid(row=1, column=1)
    result_window.mainloop()



def two_people(go_back_func):
    global root
    global theme_1
    global theme_2
    global point_1
    global point_2
    global niveau
    chiffre_list = donne_chiffre(niveau)
    change_value(chiffre_list[:7])

    root=Tk()
    theme_1 = IntVar()
    theme_2 = IntVar()
    Label(root, text='Point Rest:'+str(int(point_1)),font=('Centaur',12)).grid(row=0,column=0)
    Label(root, text='Point Rest:'+str(int(point_2)),font=('Centaur',12)).grid(row=0,column=4)
    le_but = Label(root, text='The Goal:'+str(value_signe[0]),font=('Centaur',15))
    le_but.grid(row=0,column=3)
    for i in range(5):
        Radiobutton(root,
                    text=value_signe[i + 1],
                    variable=theme_1,
                    indicatoron = 0,
                    width=20,
                    value=value_signe[i + 1]).grid(row=i + 1, column=0)
    for i in range(5):
        Radiobutton(root,
                    text=value_signe[i + 6],
                    variable=theme_1,
                    indicatoron = 0,
                    width=20,
                    value=value_signe[i + 6]).grid(row=i + 1, column=1)
    for i in range(5):
        Radiobutton(root,
                    text=value_signe[i + 11],
                    variable=theme_1,
                    indicatoron = 0,
                    width=20,
                    value=value_signe[i + 11]).grid(row=i + 1, column=2)

    for i in range(4):
        Label(root,text=' ',width=50).grid(row=i + 1, column=3)


    def go_back():
        root.destroy()
        go_back_func()

    global time_0
    time_0 = time.time()
    label_time = Label(root, text='',width=20)
    label_time.grid(row=1, column=3)
    def update_clock():
        time_now = time.time()
        label_time.configure(text='Time: ' + str(int(time_now-time_0)) + ' s')
        root.after(1000, update_clock)
    update_clock()
    Label(root,text='Niveau: '+str(niveau),width=20).grid(row=2, column=3)
    Button(root,text='Go back',width=20,command=go_back).grid(row=3, column=3)
    Button(root,text='Finish Here',width=20,command=lambda: finish_now(go_back_func)).grid(row=4, column=3)
    Button(root,text='Next Level',width=20,command=lambda: next_level(go_back_func)).grid(row=5, column=3)
    Label(root,text=' ',width=50).grid(row=6, column=3)

    for i in range(5):
        Radiobutton(root,
                    text=value_signe[i + 1],
                    variable=theme_2,
                    indicatoron = 0,
                    width=20,
                    value=value_signe[i + 1]).grid(row=i + 1, column=4)
    for i in range(5):
        Radiobutton(root,
                    text=value_signe[i + 6],
                    variable=theme_2,
                    indicatoron = 0,
                    width=20,
                    value=value_signe[i + 6]).grid(row=i + 1, column=5)
    for i in range(5):
        Radiobutton(root,
                    text=value_signe[i + 11],
                    variable=theme_2,
                    indicatoron=0,
                    width=20,
                    value=value_signe[i + 11]).grid(row=i + 1, column=6)

    #print(theme_1.get())
    theme_1.set(value_signe[le_index_1])
    #print(theme_2.get())
    theme_2.set(value_signe[le_index_2])
    root.bind('<KeyPress>',change)

    root.mainloop()


def double_play(niveau_player, point_init, go_back_func, start_window):
    global niveau, point_1, point_2, is_valide_1, is_valide_2, le_index_1, le_index_2, choix_1, choix_2
    is_valide_1 = False
    is_valide_2 = False
    le_index_1 = 1
    le_index_2 = 1
    choix_1=[]
    choix_2=[]
    start_window.destroy()
    niveau = niveau_player
    point_1 = point_init
    point_2 = point_init
    two_people(go_back_func)

#print(change_value([100,14,25,6,9,23,11]))
#two_people()
#print(choix_1,choix_2)
