from Jeu.one_player import *
from Jeu.two_player import *
from Jeu.rank import *
from Jeu.help import *

def start_play():
    start_window = Tk()
    start_window.title('心算')
    niveau_player = 1
    point = 100

    photo1 = PhotoImage(file="image1.jpg")
    photo2 = PhotoImage(file="image2.jpg")
    photo3 = PhotoImage(file="image3.jpg")
    photo4 = PhotoImage(file="image4.jpg")
    photo5 = PhotoImage(file="image5.jpg")

    Button(start_window,text="One Player",image=photo1,compound = CENTER,font=('Centaur',20),fg = "blue",
           command=lambda n=niveau_player, pt=point, go_back_func=start_play, window=start_window: single_play(n, pt, go_back_func, window)).grid()
    Button(start_window,text="Two Player",image=photo2,compound = CENTER,font=('Centaur',20),fg = "blue",
           command=lambda n=niveau_player, pt=point, go_back_func=start_play, window=start_window: double_play(n, pt, go_back_func, window)).grid()
    Button(start_window,text="Rank",image=photo3,compound = CENTER,font=('Centaur',20),fg = "blue",
           command=rank_toplevel).grid()
    Button(start_window,text="Help",image=photo4,compound = CENTER,font=('Centaur',20),fg = "blue",
           command=help_toplevel).grid()
    Button(start_window,text="Quit",image=photo5,compound = CENTER,font=('Centaur',20),fg = "blue",
           command=start_window.destroy).grid()

    start_window.mainloop()
start_play()
