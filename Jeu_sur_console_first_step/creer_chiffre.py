import random as rd


# chercher deux chiffres qui peuvent se diviser (servira a fixer le chiffre a atteindre)
def pouvoir_diviser(num_list):
    for i in range(len(num_list) - 1):
        for j in range(i + 1, len(num_list)):
            if (num_list[i] % num_list[j]) == 0:
                return [num_list[i], num_list[j]]
            elif (num_list[j] % num_list[i]) == 0:
                return [num_list[j], num_list[i]]
    return False

def operation(a, b, n): #On definit les opérations possible en fonction d'un integrer (0,1,2 ou 3)
    if n == 0:
        return a + b
    elif n == 1:
        return a - b
    elif n == 2:
        return a * b
    else:
        if b==0:
            return ZeroDivisionError
        else:
            return a / b


# ajouter les () pour + et - devant *
def traitement(list_avant):
    faut_change = False
    list_apres = ''
    for i in list_avant:
        if i == '+' or i == '-':
            faut_change = True
        if i == '*' and faut_change == True:
            list_apres = '(' + list_apres + ')' + i
        else:
            list_apres += i
    return list_apres


def donne_chiffre(niveau): # niveau doit etre plus de 0
    list_chiffre = []
    while len(list_chiffre) != 6:
        random_chiffre = rd.randint(2 * niveau, 10 * niveau)
        if random_chiffre not in list_chiffre:
            list_chiffre.append(random_chiffre)

    operation_list_1 = {0: '+', 1: '-', 2: '*', 3: '/'}
    choisi_num = rd.choice([4, 5, 6])
    operation_choice = [0, 1, 2]
    sum = 0
    resultat = ''

    if pouvoir_diviser(list_chiffre[0 : choisi_num]) == False:# ne peut pas diviser
        sum += list_chiffre[0]
        resultat += str(list_chiffre[0])

        for i in range(1, choisi_num):
            signe = rd.choice(operation_choice)
            if signe == 2 and 2 in operation_choice:# pour limiter juste une fois de fois
                operation_choice.remove(2)
            sum = operation(sum, list_chiffre[i], signe)
            resultat += operation_list_1[signe] + str(list_chiffre[i])

        rd.shuffle(list_chiffre)
        resultat = traitement(resultat)
        return [sum] + list_chiffre + [resultat]

    else:# peut diviser et on utilise seulement une division pour ne pas avoir un nombre a atteindre trop complexe
        sum = pouvoir_diviser(list_chiffre[0 : choisi_num])[0] // pouvoir_diviser(list_chiffre[0 : choisi_num])[1]
        resultat += str(pouvoir_diviser(list_chiffre[0 : choisi_num])[0]) + '/' + str(pouvoir_diviser(list_chiffre[0 : choisi_num])[1])

        reste_list = list_chiffre[0 : choisi_num]
        reste_list.remove(pouvoir_diviser(list_chiffre[0 : choisi_num])[0])
        reste_list.remove(pouvoir_diviser(list_chiffre[0 : choisi_num])[1])

        for i in range(len(reste_list) - 1):
            signe = rd.choice(operation_choice)
            if signe == 2 and 2 in operation_choice:
                operation_choice.remove(2)
            sum = operation(sum, reste_list[i], signe)
            resultat += operation_list_1[signe] + str(reste_list[i])

        rd.shuffle(list_chiffre)
        resultat = traitement(resultat)
        return [sum] + list_chiffre + [resultat]







