from Jeu.Jeu_sur_console_first_step.creer_chiffre import *
from Jeu.Jeu_sur_console_first_step.initialisation import *
import time

def game():
    compteur=100
    niveau=1
    compteur_temps=0
    while compteur>0:
        list=donne_chiffre(niveau)
        list_2=deepcopy(list)
        print('votre niveau', niveau)
        compteur_temps=time.time()
        Result=calculus(first_play(list[1:len(list)-1],list[0]))
        compteur=compteur-(abs((list[0])-Result))
        compteur_temps_bis=time.time()
        Delta_time = compteur_temps_bis - compteur_temps
        compteur=compteur-Delta_time//20
        niveau=niveau+1
        print('Vous avez mis {} secondes pour trouver le resultat'.format(Delta_time))
        print ('il fallait obtenir',list[0])
        print ('vous avez obtenu le resultat ', Result)
        print ('une des solutions possible etait :',list_2[len(list_2)-1])
        print ('votre compteur :',compteur)
    print('game over',niveau-1)

#game()
