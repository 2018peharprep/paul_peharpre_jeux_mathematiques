from tkinter import *
rule=['Rules','One Player :',
'''
You have 100 points at the beginning. 
You have to try to get a closest number referred to 
the target number with 6 numbers given and sevelral operations.
Every 10 seconds pass, you will lose 1 point. 

If you cannot achieve the target number exactly, 
you will lose the point as many as the absolute difference
between your result and the target number at each level.

If you finish one level, the level will increase, along with the difficult of the game. 
If you lose all your point, 
you cannot continue any more,and the final level will be your mark.
''','Two Players:',
'''
The first player use 
W A S D to choose the number or signe you want,and space to get your result.
The second player user 
Up Down Left Right to choose the number or signe you want, and return to get your result.
Each number can be used just once(but you can use the signe multitimes).

When both of your have finished the level you are, 
click on the button 'Next Level' to go to the next level.
If you don't want play anymore, click on the button 'Finish Here' to know who has won.
''']
def help_toplevel():
    help=Toplevel()
    Label(help,text=rule[0],width=10,font=('Centaur',25)).grid(row=0,column=0)
    Label(help,text=rule[1],width=10,font=('Centaur',15)).grid(row=1,column=0)
    Label(help,text=rule[2],width=70,font=('Centaur',14),bg="#eee4da").grid(row=2,column=0)
    Label(help,text=rule[3],width=10,font=('Centaur',15)).grid(row=3,column=0)
    Label(help,text=rule[4],width=70,font=('Centaur',14),bg="#eee4da").grid(row=4,column=0)
    help.mainloop()
