from Jeu.Jeu_sur_console_first_step.creer_chiffre import *
from tkinter import *
from tkinter.messagebox import *
from collections import Counter
from Jeu.rank import *
import time


def next_game(go_back_func):
    global is_valide, niveau, main_window, show_list, show_text, now_0, chiffre_list, button_test_list, point
    if is_valide:
        is_valide = False
        niveau += 1
        show_list = ['Operation:']
        show_text = StringVar()
        show_text.set(''.join(show_list))
        now_0 = time.time()
        chiffre_list = donne_chiffre(niveau)
        button_test_list = chiffre_list[1:7] + ['+', '-', '*', '/', '(', ')'] + ['Clean', 'Delete', 'Confirm']
        def go_back():
            global finish
            main_window.destroy()
            finish = True
            go_back_func()
        if point > 0:
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=0, column=0)
            Button(frame_cell, text='Go Back', command=go_back,width=20).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=0, column=1)
            Label(frame_cell,text='  Goal: '+str(chiffre_list[0])+' ',fg='red',font=('Centaur',15)).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=0, column=2)
            Button(frame_cell, text='Quit', command=quit_game,width=20).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=9, column=0)
            Label(frame_cell, text='Your Result: ', width=20).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=9, column=1, columnspan=2)
            Label(frame_cell, text='Solution Point: ', width=40).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=1, column=0)
            Label(frame_cell, text='Level: ' + str(niveau)).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=1, column=1)
            Label(frame_cell, text='Point Resst: ' + str(int(point))).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=2, columnspan=3)
            for i in range(5):
                for j in range(3):
                    frame_cell = Frame(main_frame)
                    frame_cell.grid(row=i + 3, column=j)
                    Button(frame_cell, text=button_test_list[i * 3 + j], width=20, bg="#eee4da",
                                command=lambda x=i * 3 + j: button_operation(str(button_test_list[x]), go_back_func)).grid()
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=8, columnspan=3)
            Label(frame_cell, textvariable=show_text, width=60).grid()
        else:
            main_window.destroy()
            result_window = Tk()
            result_window.title('Result')
            Label(result_window, text='You have reached the ' + str(niveau - 1) + ' level', width=60).grid()
            def on_click(loop_text,niveau):
                name = loop_text.get()
                rank_write(name,niveau-1)
                result_window.destroy()
                go_back_func()
            def go_back():
                result_window.destroy()
                go_back_func()
            if niveau-1>int(rank_show()[9][2]):
                Label(result_window,text="Congratulation! You have reached the 10 best niveau").grid()
                Label(result_window,text="Please enter your name:").grid()
                loop_text = StringVar()
                loop = Entry(result_window,text='Enter',textvariable = loop_text,width=15)
                loop.grid()
                Button(result_window, text="OK",width=15,command = lambda:on_click(loop_text,niveau)).grid()
                Button(result_window, text='Quit', command=result_window.destroy, width=15).grid()
            else:
                Button(result_window, text='Go Back', command=go_back, width=15).grid()
                Button(result_window, text='Quit', command=result_window.destroy, width=15).grid()
                result_window.mainloop()


def button_operation(text, go_back_func):
    global show_list, show_text, main_window, main_frame, chiffre_list, now_0, point, is_valide
    operation_list = ['+', '-', '*', '/']
    command_list = ['Clean', 'Delete', 'Confirm']
    result = chiffre_list[-1]
    text_list = Counter(show_list)

    if is_valide:
        return 0

    # les regles a suivre
    if text not in command_list:
        if show_list[-1].isdigit() and text.isdigit():  # pour eviter deux chiffres consecutifs
            return 0
        if show_list[-1] in operation_list and text in operation_list:  # pour eviter deux signes consecutifs
            return 0
        if text.isdigit() and text in show_list:  # pour eviter utiliser deux fois le meme chiffre
            return 0
        if text_list['('] == text_list[')'] and text == ')':  # le nombre de ')' doit etre moins ou egal du nombre de '('
            return 0
        if show_list[-1] == '(' and text in operation_list:  # il faut un chiffre
            return 0
        if show_list[-1] == ')' and text not in operation_list + [')']:  # if faut une signe
            return 0
        if show_list[-1] == 'Operation:' and text in operation_list + [')']:  # ce ne peut pas etre le premier element dans une equation
            return 0
        if show_list[-1].isdigit() and text == '(':  # il faut un operation
            return 0
        if show_list[-1] in operation_list + ['('] and text == ')':  # il faut un chiffre
            return 0
        show_list.append(text)
        show_text.set(''.join(show_list))
    elif text == 'Clean':
        show_list = ['Operation:']
        show_text.set(''.join(show_list))
    elif text == 'Delete':
        if show_list[-1] == 'Operation:':
            return 0
        show_list.pop()
        show_text.set(''.join(show_list))
    elif text == 'Confirm':
        if text_list['('] != text_list[')']:  # le nombre de '(' et ')' doit etre egal
            showinfo(title='Warning', message='Il faut ajouter les ) apres!')
            return 0

        is_valide = True
        time_used = time.time() - now_0

        frame_cell = Frame(main_frame)
        frame_cell.grid(row=9, column=0)
        Label(frame_cell, text='Your Result: ' + str(int(eval(''.join(show_list[1:])))), width=20).grid()
        frame_cell = Frame(main_frame)
        frame_cell.grid(row=9, column=1, columnspan=2)
        Label(frame_cell, text='Solution Possible: ' + result, width=40).grid()
        frame_time = Frame(main_frame)
        frame_time.grid(row=1, column=2)
        label_time = Label(frame_time, text='Time: ' + str(int(time_used)) + ' s', width=20)
        label_time.grid()

        point -= abs(chiffre_list[0] - int(eval(''.join(show_list[1:]))))
        point -= time_used//10

        frame_cell = Frame(main_frame)
        frame_cell.grid(row=10, columnspan=3)
        Button(frame_cell, text='Next Level', command=lambda: next_game(go_back_func)).grid()


def quit_game():
    global finish
    finish = True
    main_window.destroy()


def one_play(go_back_func):
    global main_frame, main_window
    main_window = Tk()
    main_window.title('心算')
    main_frame = Frame(main_window)
    main_frame.grid()

    global show_list, show_text
    show_list = ['Operation:']
    show_text = StringVar()
    show_text.set(''.join(show_list))

    global chiffre_list, niveau, point, button_test_list
    chiffre_list = donne_chiffre(niveau)
    button_test_list = chiffre_list[1:7] + ['+', '-', '*', '/', '(', ')'] + ['Clean', 'Delete', 'Confirm']

    def go_back():
        global finish
        main_window.destroy()
        finish = True
        go_back_func()

    # create the button 'go back' and 'quit'
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=0, column=0)
    Button(frame_cell, text='Go Back', command=go_back,width=20).grid()
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=0, column=1)
    Label(frame_cell, text='  Goal: '+str(chiffre_list[0])+' ',fg='red',font=('Centaur',15)).grid()
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=0, column=2)
    Button(frame_cell, text='Quit', command=quit_game,width=20).grid()

    # quit the label 'level' and ' pt rest'
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=1, column=0)
    Label(frame_cell, text='Level: ' + str(niveau)).grid()
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=1, column=1)
    Label(frame_cell, text='Point Rest: ' + str(int(point))).grid()

    #create the label of time
    global now_0
    now_0 = time.time()
    frame_time = Frame(main_frame)
    frame_time.grid(row=1, column=2)
    label_time = Label(frame_time, text='', width=20)
    label_time.grid()
    def update_clock():
        now = time.time()
        label_time.configure(text='Time: ' + str(int(now-now_0)) + ' s')
        frame_time.after(1000, update_clock)
    update_clock()

    #create the main part of the game
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=2, columnspan=3)

    for i in range(5):
        for j in range(3):
            frame_cell = Frame(main_frame)
            frame_cell.grid(row=i + 3, column=j)
            Button(frame_cell, text=button_test_list[i * 3 + j], width=20, bg="#eee4da",
                        command=lambda x=i * 3 + j: button_operation(str(button_test_list[x]), go_back_func)).grid()
    frame_cell = Frame(main_frame)
    frame_cell.grid(row=8, columnspan=3)
    Label(frame_cell, textvariable=show_text, width=60).grid()

    main_window.mainloop()


def single_play(niveau_player, point_init, go_back_func, start_window):
    global niveau, point, finish, close, have_show_result, is_valide
    finish = False
    close = False
    have_show_result = False
    is_valide = False
    start_window.destroy()
    niveau = niveau_player
    point = point_init
    one_play(go_back_func)

