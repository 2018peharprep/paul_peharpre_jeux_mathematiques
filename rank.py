import csv
from tkinter import *

global filename
filename='/Users/lichen/PycharmProjects/codingweek_final/rank.csv'

def rank_show():
    with open(filename,newline='') as csvfile:
        reader=csv.reader(csvfile,delimiter=';')
        rank_list=[]
        for row in reader:
            rank_list.append(row)
    return rank_list

def change_rank_list(rank_list,name,score):
        for the_rank in range(len(rank_list)-1):
            if score<=int(rank_list[the_rank][2]) and score>int(rank_list[the_rank+1][2]):
                rank_list.insert(the_rank+1,[str(the_rank+2),name,str(score)])
                rank_list.pop(-1)
                break
            else:
                continue
        for i in range(the_rank+2,len(rank_list)):
            rank_list[i][0]=str(int(rank_list[i][0])+1)
        return rank_list

def rank_write(name,score):
    rank_list=change_rank_list(rank_show(),name,score)
    with open(filename,'w',newline='') as csvfile:
        file_writer=csv.writer(csvfile,delimiter=';')
        for i in range(len(rank_list)):
            file_writer.writerow(rank_list[i])
#rank_write('time',70)



def rank_toplevel():
    global filename
    rank_top=Toplevel()
    color =["#f67c5f","#f1b078","#eb8c52","#9e948a","#9e948a","#9e948a","#9e948a","#9e948a","#9e948a","#9e948a"]
    Label(rank_top,text='Rank',width=8,font=('Centaur',18)).grid(row=0,column=0)
    Label(rank_top,text='Name',width=8,font=('Centaur',18)).grid(row=0,column=1)
    Label(rank_top,text='Score',width=8,font=('Centaur',18)).grid(row=0,column=2)
    for i in range(10):
        Label(rank_top,text=rank_show()[i][0],width=12,font=('Centaur',15),bg=color[i]).grid(row=i+1,column=0)
        Label(rank_top,text=rank_show()[i][1],width=12,font=('Centaur',15),bg=color[i]).grid(row=i+1,column=1)
        Label(rank_top,text=rank_show()[i][2],width=12,font=('Centaur',15),bg=color[i]).grid(row=i+1,column=2)
    rank_top.mainloop()
#rank_toplevel(filename)
