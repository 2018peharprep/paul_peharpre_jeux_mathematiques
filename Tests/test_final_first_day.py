from Jeu.Jeu_sur_console_first_step.creer_chiffre import *
from Jeu.Jeu_sur_console_first_step.initialisation import *



# Test des fonctions de creer_chiffre

def test_pouvoir_diviser():
    assert pouvoir_diviser([6,2])==[6,2]
    assert pouvoir_diviser([2,6])==[6,2]
    assert pouvoir_diviser([12,6,4,2])==[12,6]
    assert pouvoir_diviser([14,12,6,4,2])==[14,2]
    assert pouvoir_diviser([4,5])==False

def test_operation():
    assert (operation(0,1,0)==1)
    assert (operation(0,1,1)==-1)
    assert (operation(0,1,2)==0)
    assert (operation(1,0,3)==ZeroDivisionError)
def test_donne_chiffre():
    for i in donne_chiffre(4)[1:7]:
        assert (i<=40 and i>=4)


# Test Initialisation

def test_calculus():
    assert calculus([1,'+',5,'/',3,'*',7])==14
    assert calculus([1,'+',5,'/',0,'*',7])==ZeroDivisionError
    assert calculus([])==0
#def first_play():
