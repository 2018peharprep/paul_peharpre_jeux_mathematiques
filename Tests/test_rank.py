import pytest
from Jeu.rank import *

rank=[['1','robin','100'],['2','robun','90'],['3','roban','50'],['4','','0'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']]
def test_change_rank_list():
    assert change_rank_list([['1','robin','100'],['2','robun','90'],['3','roban','50'],['4','','0'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']],'justin',100)==[['1','robin','100'],['2','justin','100'],['3','robun','90'],['4','roban','50'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']]
    assert change_rank_list([['1','robin','100'],['2','robun','90'],['3','roban','50'],['4','','0'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']],'lala',-10)==[['1','robin','100'],['2','robun','90'],['3','roban','50'],['4','','0'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']]
    assert change_rank_list([['1','robin','100'],['2','robun','90'],['3','roban','50'],['4','','0'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']],'lala',80)==[['1','robin','100'],['2','robun','90'],['3','lala','80'],['4','roban','50'],['5','','0'],['6','','0'],['7','','0'],['8','','0'],['9','','0'],['10','','0']]
test_change_rank_list()
